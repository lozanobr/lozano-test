// ================================================
// Note to candidate:
//	This code has bad practices
//	feel free to replace it with a better solution
// ================================================

var API_HOST = "freegeoip.net";

function updateLocationDetails(data){
	var now = new Date();

	$("#location_query").html(data.ip);
	$("#location_country").html(data.country_name);
	$("#location_regionName").html(data.region_name);
	$("#location_city").html(data.city);
	$("#location_zip_code").html( data.zip_code);
	$("#location_lat").html(data.latitude);
	$("#location_lon").html(data.longitude);

	$("table").removeClass("empty");
	$(".help").click(function(e){
		var fieldName = $(e.currentTarget).closest('tr').find('.field_name').text();
		alert( "This is your " + fieldName + " according to " + API_HOST + " at " + now );
	});
}

function getMyLocation() {
	$.ajax({
		type : 'GET',
		url : 'http://' + API_HOST + '/json/',
		success : function(response){
			updateLocationDetails(response);
		}
	});
}


function resetLocationDetails() {
	updateLocationDetails({
		ip: "0.0.0.0",
		country_name: "",
		region_name: "",
		city: "",
		zip_code: "",
		latitude: "",
		longitude: ""
	});
	$("table").addClass("empty");
}

function searchLocationMap(data){

	var lat = data.latitude;
	var lng = data.longitude;

	$('.latitude').text(lat.toFixed(3));
	$('.longitude').text(lng.toFixed(3));
	$('.coordinates').addClass('visible');

	var map = new GMaps({
	  el: '#map',
	  lat: lat,
	  lng: lng
	});

	map.addMarker({
	  lat: lat,
	  lng: lng
	});
}

function initializePage(){

	function initMap() {
        var uluru = {lat: -23.556612, lng: -46.659557};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
    }

	initMap();

	window.indexTemplate = $('#index').html();
	window.locationTemplate = $('#locationInfo').html();

	window.indexTemplate = Handlebars.compile(window.indexTemplate);
	window.locationTemplate = Handlebars.compile(window.locationTemplate);

	$("#mainContent").html(window.indexTemplate());
	$("#geoLocationContainer").html(window.locationTemplate({
		id: 0,
		ip: "0.0.0.0",
		country_name: "",
		region_name: "",
		city: "",
		zip_code: "",
		latitude: "",
		longitude: ""
	}));


	var findMeButton = $('.find-me');

	if (!navigator.geolocation) {
	
	  findMeButton.addClass("disabled");
	  $('.no-browser-support').addClass("visible");
	
	} else {
	
	  findMeButton.on('click', function(e) {
	
		e.preventDefault();
	
		navigator.geolocation.getCurrentPosition(function(position) {

		  var lat = position.coords.latitude;
		  var lng = position.coords.longitude;
	
		  $('.latitude').text(lat.toFixed(3));
		  $('.longitude').text(lng.toFixed(3));
		  $('.coordinates').addClass('visible');
	
		  var map = new GMaps({
			el: '#map',
			lat: lat,
			lng: lng
		  });
	
		  map.addMarker({
			lat: lat,
			lng: lng
		  });
	
		});
	
	  });
	
	}

	$("#getUrlLocation").submit(function(e) {	
		
		var textUrl = $("#url").val();
		$.ajax({
			type: "GET",
			url : 'http://' + API_HOST + '/json/'+ textUrl,
			data: $(this).serialize(),
			success: function(data) {
				updateLocationDetails(data);
				searchLocationMap(data);
				console.log(data);
			}
	   });
	   e.preventDefault();
	});

}

$(document).ready(function(){
	initializePage();
});
